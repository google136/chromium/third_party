# DelayAsyncScriptExecution
This suite runs the tests in wpt_internal/async-script-scheduling/ with
`--enable-features=DelayAsyncScriptExecution:target/cross_site_with_allow_list/allow_list/http%3A%2F%2Fbad3p.test`.

See crbug.com/1340837.
