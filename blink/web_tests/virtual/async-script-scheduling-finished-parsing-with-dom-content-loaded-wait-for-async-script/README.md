# DelayAsyncScriptExecution
This suite runs the tests in wpt_internal/async-script-scheduling/ with
`--enable-features=DelayAsyncScriptExecution:delay_type/finished_parsing/limit/1m,DOMContentLoadedWaitForAsyncScript`.

See crbug.com/1340837.
