This is a testharness.js-based test.
PASS e.style['color'] = "color-contrast(white vs red, blue)" should set the property value
PASS e.style['color'] = "color-contrast(white vs blue, red)" should set the property value
PASS e.style['color'] = "color-contrast(white vs red, blue, green)" should set the property value
PASS e.style['color'] = "color-contrast(white vs white, white)" should set the property value
PASS e.style['color'] = "color-contrast(blue vs red, white)" should set the property value
PASS e.style['color'] = "color-contrast(red vs blue, white, red)" should set the property value
PASS e.style['color'] = "color-contrast(black vs red, blue)" should set the property value
PASS e.style['color'] = "color-contrast(black vs blue, red)" should set the property value
PASS e.style['color'] = "color-contrast(black vs white, white)" should set the property value
PASS e.style['color'] = "color-contrast(red vs blue, rgb(255, 255, 255, .5))" should set the property value
PASS e.style['color'] = "color-contrast(wheat vs bisque, darkgoldenrod, olive, sienna, darkgreen, maroon to AA)" should set the property value
PASS e.style['color'] = "color-contrast(wheat vs bisque, darkgoldenrod, olive, sienna, darkgreen, maroon to AA-large)" should set the property value
PASS e.style['color'] = "color-contrast(wheat vs bisque, darkgoldenrod, olive, sienna, darkgreen, maroon to AAA)" should set the property value
PASS e.style['color'] = "color-contrast(wheat vs bisque, darkgoldenrod, olive, sienna, darkgreen, maroon to AAA-large)" should set the property value
PASS e.style['color'] = "color-contrast(wheat vs bisque, darkgoldenrod, olive, sienna, darkgreen, maroon to 5.8)" should set the property value
PASS e.style['color'] = "color-contrast(wheat vs bisque, darkgoldenrod, olive to 100)" should set the property value
PASS e.style['color'] = "color-contrast(green vs bisque, darkgoldenrod, olive to 100)" should set the property value
PASS e.style['color'] = "color-contrast(green vs color(display-p3 0 1 0), color(display-p3 0 0 1))" should set the property value
FAIL e.style['color'] = "color-contrast(color(display-p3 1 1 0) vs color(display-p3 0 1 0), color(display-p3 0 0 1))" should set the property value assert_equals: serialization should be canonical expected "color(display-p3 0 0 1)" but got "color(display-p3 0 1 0)"
FAIL e.style['color'] = "color-contrast(green vs lab(50% -160 160), lch(20% 50 20deg))" should set the property value assert_equals: serialization should be canonical expected "lch(20% 50 20)" but got "lab(50% -160 160)"
FAIL e.style['color'] = "color-contrast(lab(50% -160 160) vs green, lch(20% 50 20deg))" should set the property value assert_equals: serialization should be canonical expected "lch(20% 50 20)" but got "rgb(0, 128, 0)"
PASS e.style['color'] = "color-contrast( white vs red, blue )" should set the property value
Harness: the test ran to completion.

